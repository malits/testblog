<?php

namespace App\QueryFilters;

class PostFilter extends BaseQueryFilter
{
    public function search(string $value): void
    {
        $this->builder->where('title', 'like', "%{$value}%");
    }

}
