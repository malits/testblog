<?php

namespace App\Models;

use App\QueryFilters\Filterable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory, Filterable;

    protected $fillable = ['title', 'description', 'content', 'user_id'];

    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }
}
