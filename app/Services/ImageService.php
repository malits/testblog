<?php

namespace App\Services;

use Spatie\Image\Image;
use Spatie\Image\Image as ImageFasade;

class ImageService
{
    public function resizeImage($image, int $width, int $height)
    {
        ImageFasade::load($image)
            ->width($width)
            ->height($height)->save();
    }
}

