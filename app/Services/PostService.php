<?php

namespace App\Services;

use App\Models\Image;
use App\Models\Post;
use Illuminate\Http\UploadedFile;

class PostService
{
    /**
     * @var ImageService
     */
    private $imageService;

    public function __construct(ImageService $imageService)
    {
        $this->imageService = $imageService;
    }

    public function save(array $attributes): void
    {
        $attributes['user_id'] = request()->user()->id;

        $post = Post::create($attributes);
        $path = $this->saveImage(request()->file('image'));
        $post->image()->save(
            Image::make(['path' => $path])
        );
    }

    public function update(Post $post, array $attributes): void
    {
        $attributes['user_id'] = request()->user()->id;

        $post->update($attributes);
        $path = $this->saveImage(request()->file('image'));

        $post->image()->save(
            Image::make(['path' => $path])
        );
    }

    private function saveImage($file): string
    {
        //Todo вынести в конфиги параметры картинок и путь их расположения
        $this->imageService->resizeImage($file, 640, 480);
        $image = new UploadedFile($file->path(), $file->getClientOriginalName());
        return $image->store('posts_images');
    }
}
