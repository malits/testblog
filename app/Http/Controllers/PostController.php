<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostRequest;
use App\Models\Post;
use App\QueryFilters\PostFilter;
use App\Services\PostService;

class PostController extends Controller
{
    /**
     * @var PostService
     */
    private $postService;

    public function __construct(PostService $postService)
    {
        $this->postService = $postService;
    }

    public function index(PostFilter $filters)
    {
        $posts = Post::with('image')
            ->filter($filters)
            ->latest()
            ->simplePaginate(10);
        return view('posts.index', compact('posts'));
    }

    public function create()
    {
        return view('posts.create');
    }

    public function store(PostRequest $request)
    {
        $this->postService->save($request->validated());
        $request->session()->flash('status', 'Post was created!');

        return redirect()->route('posts.index');
    }

    public function show(Post $post)
    {
        return view('posts.show', compact('post'));
    }

    public function edit(Post $post)
    {
        return view('posts.edit', compact('post'));
    }

    public function update(PostRequest $request, Post $post)
    {
        $this->postService->update($post, $request->validated());
        $request->session()->flash('status', 'Post was updated!');

        return redirect()->route('posts.index');
    }

    public function destroy(Post $post)
    {
        $post->delete();
        request()->session()->flash('status', 'Post was deleted!');

        return redirect()->route('posts.index');
    }
}
