<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required', 'unique:posts', 'max:100'],
            'description' => ['required', 'max:200'],
            'content' => ['required'],
            'image' => ['required', 'image', 'mimes:jpeg,png,jpg,gif,svg,', 'dimensions:min_width=640,min_height=480']
        ];
    }
}
