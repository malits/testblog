@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">{{ __('Edit Post') }}</div>
                    <div class="card-body">
                        <form action="{{route('posts.update', $post->id)}}" method="post"
                              enctype="multipart/form-data">
                            @csrf
                            @method('PUT')

                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" name="title"
                                       value="{{$post->title}}"
                                       class="form-control"
                                       id="title"
                                       required>
                            </div>
                            <div class="form-group">
                                <label for="description">Short description</label>
                                <input type="text" name="description" value="{{$post->description}}"
                                       class="form-control" id="description" required>
                            </div>
                            <div class="form-group">
                                <label for="content">Content</label>
                                <textarea class="form-control" id="content" name="content" required>
                                    {{$post->content}}
                                </textarea>
                            </div>
                            <div class="form-group">
                                <label for="file">Image</label>
                                <input type="file" name="image"
                                       value="{{$post->image->path}}"
                                       class="form-control-file" required
                                       id="file">
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
@endsection
