@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">{{ __('Posts') }}</div>
                    @if(Session::has('status'))
                        <div class="alert alert-success">
                            {{ Session::get('status')}}
                        </div>
                    @endif
                    <div class="card-body">
                        @forelse ($posts as $post)
                            <a href="{{route('posts.show', $post->id)}}"><h2>{{ $post->title }}</h2></a>
                            <p>{{$post->description}}</p>
                        @empty
                            <p>No posts</p>
                        @endforelse
                    </div>
                    {{$posts->links()}}
                </div>
            </div>
            <div class="col-md-2">
                <div class="card">
                    <div class="card-header"><a href="{{route('posts.create')}}">Add new post</a></div>
                </div>
            </div>
        </div>
    </div>
@endsection
