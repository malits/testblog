@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">{{ __('Add new Post') }}</div>

                    <div class="card-body">
                        <form action="{{route('posts.store')}}" method="post"
                              enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" name="title"
                                       value="{{old('title')}}"
                                       class="form-control"
                                       id="title"
                                       required>
                            </div>
                            <div class="form-group">
                                <label for="description">Short description</label>
                                <input type="text" name="description" value="{{old('description')}}"
                                       class="form-control" id="description" required>
                            </div>
                            <div class="form-group">
                                <label for="content">Content</label>
                                <textarea class="form-control" id="content" name="content" required></textarea>
                            </div>
                            <div class="form-group">
                                <label for="file">Image</label>
                                <input type="file" name="image" class="form-control-file" required
                                       id="file">
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
