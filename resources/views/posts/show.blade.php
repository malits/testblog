@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">
                        <a href="{{route('posts.edit', $post->id)}}">
                            <h2>{{ $post->title }}</h2>
                        </a>
                        @include('posts.destroy')
                    </div>
                    <div class="card-body">
                        {{--                        {{$post->image->path}}--}}

                        <img src="{{\Illuminate\Support\Facades\Storage::disk('public')->url($post->image->path)}}"
                             alt="{{$post->title}}">
                        <p>{{$post->description}}</p>
                        <div class="ya-share2" data-services="vkontakte,twitter,facebook,gplus"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
